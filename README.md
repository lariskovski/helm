## Helm Charts repo

Example on how to serve Helm Charts via Gitlab pages.

## Usage

Add Helm repo:

``helm repo add my-repo https://<group-name>.gitlab.io/<project-name>``

Use Helm repo:

``helm install my-repo/test``

## sources

### Getting started with Helm

[First Helm Chart - Bitnami](https://docs.bitnami.com/kubernetes/how-to/create-your-first-helm-chart/)

[Gitab CI deploy with Helm](https://about.gitlab.com/blog/2017/09/21/how-to-create-ci-cd-pipeline-with-autodeploy-to-kubernetes-using-gitlab-and-helm/)

[Hosting Helm charts on Gitlab Pages](https://tobiasmaier.info/posts/2018/03/13/hosting-helm-repo-on-gitlab-pages.html)